package com.example.todoproject

import android.app.Application
import android.content.Context

class MyTODO : Application(){
    override fun onCreate() {
        super.onCreate()
        MyTODO.context = applicationContext
    }

    companion object {
        private lateinit var context: Context

        fun gContext(): Context {
            return context
        }
    }
}