package com.example.todoproject

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class TodoFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val mA = MainActivity()
        val list = mA.getList()
        val id = mA.index
        val view = inflater.inflate(R.layout.todo_fragment, container, false)
        val text = view.findViewById<TextView>(R.id.tv_todo)
        text.text = getTextTodo()
        val btnCheak = view.findViewById<Button>(R.id.btn_checked)
        val btnError = view.findViewById<Button>(R.id.btn_error)
        val btnPencil = view.findViewById<Button>(R.id.btn_pencil)
        return view
    }

    private fun getTextTodo(): String = tv_text.text.toString()

    companion object {
        @JvmStatic
        fun newInstance(): TodoFragment {
            return TodoFragment()
        }

    }

}