package com.example.todoproject.model


data class TodoObject(
    val id: Int,
    val text : String
)