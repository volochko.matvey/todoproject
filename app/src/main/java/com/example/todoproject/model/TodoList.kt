package com.example.todoproject.model


import com.example.todoproject.MyTODO
import java.util.ArrayList

object TodoList {

    val ITEMS: MutableList<TodoObject> = ArrayList()
    var id: Int = 0
    var text = ""

    init {
        MyTODO.gContext().assets?.list("")?.forEach {
            ITEMS.add(TodoObject(id, text))
        }
    }
}
