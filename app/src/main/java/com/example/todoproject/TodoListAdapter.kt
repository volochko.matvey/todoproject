package com.example.todoproject

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todoproject.model.TodoObject
import kotlinx.android.synthetic.main.activity_main.view.*

class TodoListAdapter(

    private val list: MutableList<TodoObject>
) : RecyclerView.Adapter<TodoListAdapter.ViewHolder>() {

    private var idTodo: Int = 0

    override fun getItemCount(): Int = list.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.todo_fragment, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.textTodo.text = item.text
        idTodo = item.id
        holder.btnCheak.setOnClickListener {
            list.removeAt(position)
            TodoListAdapter(list).notifyItemRemoved(position)
            MainActivity().index = position
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textTodo = view.findViewById<TextView>(R.id.tv_todo)
        val textText = view.findViewById<TextView>(R.id.tv_text)
        val btnError = view.findViewById<Button>(R.id.btn_error)
        val btnPencil = view.findViewById<Button>(R.id.btn_pencil)
        val btnCheak = view.findViewById<Button>(R.id.btn_checked)
    }

}