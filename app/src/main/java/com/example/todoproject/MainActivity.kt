package com.example.todoproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoproject.model.TodoObject
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.todo_fragment.*

class MainActivity : AppCompatActivity() {
    var index = 0
    private val values = mutableListOf<TodoObject>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView = findViewById<RecyclerView>(R.id.list)
        val fragment = TodoFragment()
        btn_ok.setOnClickListener {
            val textView: TextView = findViewById(R.id.tv_text)
            values.add(index, TodoObject(
                index, textView.text.toString()
            )
            )
            recyclerView.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
            recyclerView.adapter = TodoListAdapter(getList())
            index++
            supportFragmentManager.beginTransaction().replace(R.id.list, TodoFragment.newInstance())
        }
    }

    fun getList():  MutableList<TodoObject> {
        return values
    }

}


